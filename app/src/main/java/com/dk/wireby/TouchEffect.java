package com.dk.wireby;

import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

// TODO: Auto-generated Javadoc

/**
 * The Class TouchEffect will use for touch effect on view
 */
public class TouchEffect implements OnTouchListener
{

	/* (non-Javadoc)
	 * @see android.view.View.OnTouchListener#onTouch(android.view.View, android.view.MotionEvent)
	 */
	@SuppressWarnings("deprecation")
	@Override
	public boolean onTouch(View v, MotionEvent event)
	{
		if (event.getAction() == MotionEvent.ACTION_DOWN)
		{
			v.setAlpha(0.6f);
		}
		else if (event.getAction() == MotionEvent.ACTION_UP
				|| event.getAction() == MotionEvent.ACTION_CANCEL)
		{
			v.setAlpha(1f);
		}

		return false;
	}

}
