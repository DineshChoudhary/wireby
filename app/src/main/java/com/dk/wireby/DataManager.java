package com.dk.wireby;

import com.loopj.android.http.RequestParams;

/**
 * Created by Logictrix on 20-Sep-18.
 */

public class DataManager {
    public RequestParams params;
    public static DataManager dataManager;
    public static DataManager getInstanse(){
        if(dataManager == null)
            return dataManager = new DataManager();
        else
            return dataManager;
    }
    public RequestParams getTableSaveOrderParams(){
        return params;
    }
    public void setTableSaveOrderParams(RequestParams params){
        this.params = params;
    }
}
