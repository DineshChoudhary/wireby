package com.dk.wireby;

import android.content.Context;
import android.content.Intent;
import android.os.Process;
import java.io.PrintWriter;
import java.io.StringWriter;

public class ExceptionHandler implements Thread.UncaughtExceptionHandler {

	/** The my context. */
	private final Context myContext;

	/**
	 * Instantiates a new exception handler.
	 *
	 * @param context the context
	 */
	public ExceptionHandler(Context context)
	{

		myContext = context;
	}

	/** The Constant CRASH_REPORT. */
	public static final String CRASH_REPORT = "crashReport";

	/* (non-Javadoc)
	 * @see java.lang.Thread.UncaughtExceptionHandler#uncaughtException(java.lang.Thread, java.lang.Throwable)
	 */
	@Override
	public void uncaughtException(Thread thread, Throwable exception)
	{

		final StringWriter stackTrace = new StringWriter();
		exception.printStackTrace(new PrintWriter(stackTrace));

		System.err.println(stackTrace);// You can use LogCat too

		Intent intent = new Intent(myContext, SplashActivity.class);		//**
		intent.putExtra(CRASH_REPORT, stackTrace.toString());
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		myContext.startActivity(intent);

		Process.killProcess(Process.myPid());
		System.exit(10);
	}


}
