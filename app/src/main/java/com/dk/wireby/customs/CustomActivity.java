package com.dk.wireby.customs;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import com.dk.wireby.ExceptionHandler;
import com.dk.wireby.TouchEffect;
import com.loopj.android.http.AsyncHttpClient;

public class CustomActivity extends AppCompatActivity implements OnClickListener {

    public static final TouchEffect TOUCH = new TouchEffect();
    public CustomActivity THIS;
    AsyncHttpClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        THIS = this;
    }

    public View setTouchNClick(int id) {
        View v = setClick(id);
        if (v != null)
            v.setOnTouchListener(TOUCH);
        return v;
    }

    public View setTouchNClick(View v) {
        if (v != null) {
            v.setOnTouchListener(TOUCH);
            setClick(v);
        }
        return v;
    }

    public View setClick(int id) {
        View v = findViewById(id);
        if (v != null)
            v.setOnClickListener(this);
        return v;
    }

    public View setClick(View v) {
        if (v != null)
            v.setOnClickListener(this);
        return v;
    }

    @Override
    public void onClick(View v) {
    }

    public String stripHtml(String html) {
        return String.valueOf(Html.fromHtml(html));
    }

    public static boolean isEmpty(String str) {
        return str == null || str.trim().equalsIgnoreCase("null")
                || str.trim().length() == 0;
    }

}
