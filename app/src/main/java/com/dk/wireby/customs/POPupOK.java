package com.dk.wireby.customs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.widget.TextView;
import com.dk.wireby.R;

/**
 * Created by Dinesh Choudhary on 24-oct-17.
 */
public class POPupOK extends CustomActivity{

    private static POPupOK instance;
    private Context mContext;
    private Dialog customDialog;
    Dialogclick dialogclick;
    TextView action_OK,title11;
    String title;

    public static POPupOK getInstance() {
        if (instance == null) {
            instance = new POPupOK();
        }
        return instance;
    }

    public void createDialog(Context mContext, String title, Dialogclick dialogclick) {
        this.mContext = mContext;
        this.dialogclick = dialogclick;
        this.title =title;
        customDialog = new Dialog(mContext);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        customDialog.setContentView(R.layout.popupok);
        customDialog.setCanceledOnTouchOutside(false);
        customDialog.show();
        init();
    }

    private void init() {
        title11 = (TextView)customDialog.findViewById(R.id.title11);
        title11.setText(title);
        action_OK = (TextView)customDialog.findViewById(R.id.action_yes);
        setTouchNClick(action_OK);
    }

    public interface Dialogclick {
        public void onClick();
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        if(v==action_OK){
            customDialog.dismiss();
            dialogclick.onClick();
        }
    }
}
