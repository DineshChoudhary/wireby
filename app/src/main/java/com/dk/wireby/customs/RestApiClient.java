package com.dk.wireby.customs;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.dk.wireby.AppController;
import com.dk.wireby.Constants;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import org.json.JSONException;
import org.json.JSONObject;
import cz.msebera.android.httpclient.Header;

import static com.dk.wireby.Constants.MY_SOCKET_TIMEOUT_MS;

/**
 * Created by Logictrix on 12-Sep-18.
 */

public class RestApiClient {
    final public static int POST = 1;
    final public static int GET = 2;
    AsyncHttpClient client;
    Context context;
    int METHOD = 1;
    String url, loadingMsg;
    RequestParams params;
    ResponseListener responseListener;
    String url1;

    public RestApiClient(Context c, int method, String url, RequestParams params, String loadingMsg, ResponseListener listener) {
        this.context = c;
        this.METHOD = method;
        this.url = url;
        this.params = params;
        this.loadingMsg = loadingMsg;
        this.responseListener = listener;
        RestApiCall();
    }

    private void RestApiCall() {
        if(!loadingMsg.equalsIgnoreCase("notshow"))
            AppController.spinnerStart(context, loadingMsg);
        client = new AsyncHttpClient();
//        try {
//            client.addHeader("x-custid", "" + AppController.getInstance().readActiveData().getOmegaCustomerid());
//            client.addHeader("x-workstation", "" + AppController.getInstance().readActiveData().getWorkstationNumber());
//        } catch (Exception ee) {
//        }
//        client.addHeader("x-deviceid", "" + AppController.getDeviceId());
        client.setResponseTimeout(MY_SOCKET_TIMEOUT_MS);
        url1 = ""+ Constants.BASE_URL.replace("@@", url);

        client.get(context, url1, params, new TextHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                if(!loadingMsg.equalsIgnoreCase("notshow"))
                    AppController.spinnerStop();
                try {
                    JSONObject response = new JSONObject(responseString);
                    ActionSuccess(response, headers);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                if(!loadingMsg.equalsIgnoreCase("notshow"))
                    AppController.spinnerStop();
                try {
                    JSONObject response = new JSONObject(responseString);
                    ActionFailure(response, throwable);
                } catch (Exception e) {
                    e.printStackTrace();
                    ActionFailure(null, throwable);
                }
            }
        });
    }

    public interface ResponseListener {
        void success(JSONObject response, Header[] headers);

        void showError(String message);
    }

    private void ActionSuccess(JSONObject response, Header[] headers) {
        if (responseListener != null)
            responseListener.success(response, headers);
        else {
            POPupOK.getInstance().createDialog(context, "Please Retry:\n", new POPupOK.Dialogclick() {
                @Override
                public void onClick() {
                }
            });
        }
    }

    private void ActionFailure(JSONObject response, Throwable throwable) {
        String excep = "";
        try {
            excep = "" + (response.getJSONObject("response")).getString("exception");
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (excep.equals("NotAuthorizedException")) {
            Toast.makeText(context, "NotAuthorizedException", Toast.LENGTH_SHORT).show();
        } else if (excep.equals("NotActivatedException")) {
            Toast.makeText(context, "NotActivatedException", Toast.LENGTH_SHORT).show();
        } else {
            if (throwable.toString().contains("UnknownHostException"))
                responseListener.showError("No Internet Connection !!");
            else if (throwable.toString().contains("TimeoutException"))
                responseListener.showError("Server Timeout !!");
            else
                responseListener.showError("" + throwable.toString());
        }
    }

    public void actionError(Intent intent) {
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        context.startActivity(intent);
        ((Activity)context).finish();
    }
}
