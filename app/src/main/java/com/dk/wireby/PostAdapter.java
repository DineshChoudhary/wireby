package com.dk.wireby;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.List;

/**
 * Created by android3 on 05-Jul-17.
 */

public class PostAdapter extends RecyclerView.Adapter<PostAdapter.RecyclerViewHolder> {

    private List<PostPojo> arrayList;
    private Activity activity;
    RecyclerViewHolder listHolder;

    public PostAdapter(Activity activity, List<PostPojo> rowListItem) {
        this.activity = activity;
        this.arrayList = rowListItem;
    }

    @Override
    public int getItemCount() {
        return (null != arrayList ? arrayList.size() : 0);

    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        holder.title.setText("" + arrayList.get(position).getTitle());
        holder.desc.setText("" + arrayList.get(position).getDesc());
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        LayoutInflater mInflater = LayoutInflater.from(viewGroup.getContext());

        ViewGroup mainGroup = (ViewGroup) mInflater.inflate(R.layout.discover_row, viewGroup, false);
        listHolder = new RecyclerViewHolder(mainGroup);

        return listHolder;
    }

    public static class RecyclerViewHolder extends RecyclerView.ViewHolder {

        public TextView title, desc;

        public RecyclerViewHolder(View itemview) {
            super(itemview);
            this.title = (TextView) itemview.findViewById(R.id.titles);
            this.desc = (TextView) itemview.findViewById(R.id.desc);
        }
    }

}
