package com.dk.wireby;

/**
 * Created by nm on 10/11/2018.
 */

public class PostPojo {
    String title;
    String desc;

    public PostPojo(String title, String desc){
        this.title =title;
        this.desc =desc;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
