package com.dk.wireby;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.TextView;


/**
 * Created by logictrix on 25-Sep-17.
 */
public class AddValueDialog extends AppCompatActivity{
    private static AddValueDialog instance;
    private Context mContext;
    private Dialog customDialog;
    TextView btn_save;

    public static AddValueDialog getInstance() {
        if (instance == null) {
            instance = new AddValueDialog();
        }
        return instance;
    }

    public void createDialog(Context mContext) {
        this.mContext = mContext;
        customDialog = new Dialog(mContext);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        customDialog.setContentView(R.layout.adddialog_row);
        customDialog.setCanceledOnTouchOutside(true);
        customDialog.show();
        init();
    }

    private void init() {
        btn_save = (TextView)customDialog.findViewById(R.id.cancel);
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customDialog.dismiss();
            }
        });
    }

}
