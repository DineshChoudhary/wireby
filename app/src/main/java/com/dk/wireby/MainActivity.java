package com.dk.wireby;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;
import com.dk.wireby.customs.CustomActivity;

public class MainActivity extends CustomActivity {

    private ViewPager viewPager;
    private TabLayout tabLayout;
    ImageView menuIcon,notify;
    TextView deleteView,expiredView,customizeView;
    ActionBarDrawerToggle mDrawerToggle;
    DrawerLayout drawerLayout;
    NavigationView navigationView1;
    private int[] tabIcons = {
            R.drawable.ic_chats,
            R.drawable.ic_add_buddy_header,
            R.drawable.ic_chats,
            R.drawable.ic_add_buddy_header
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        inIt();
    }

    private void inIt() {
        menuIcon = findViewById(R.id.menuIcon);
        setTouchNClick(menuIcon);
        notify = findViewById(R.id.notify);
        setTouchNClick(notify);
        deleteView = findViewById(R.id.deleteView);
        setTouchNClick(deleteView);
        expiredView = findViewById(R.id.expiredView);
        setTouchNClick(expiredView);
        customizeView = findViewById(R.id.customizeView);
        setTouchNClick(customizeView);

        //TODO: UI DRAWER
        drawerLayout = findViewById(R.id.drawer_layout);
        navigationView1 = findViewById(R.id.navigation);
        mDrawerToggle = new ActionBarDrawerToggle(MainActivity.this, drawerLayout, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

        viewPager = findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                tab.getIcon().setColorFilter(Color.BLUE, PorterDuff.Mode.SRC_IN);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                tab.getIcon().setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_IN);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void setupTabIcons() {
        tabLayout.getTabAt(0).setIcon(tabIcons[0]).getIcon().setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_IN);
        tabLayout.getTabAt(1).setIcon(tabIcons[1]).getIcon().setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_IN);
        tabLayout.getTabAt(2).setIcon(tabIcons[2]).getIcon().setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_IN);
        tabLayout.getTabAt(3).setIcon(tabIcons[3]).getIcon().setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_IN);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new taball(), "");
        adapter.addFrag(new taball(), "");
        adapter.addFrag(new taball(), "");
        adapter.addFrag(new taball(), "");
        viewPager.setAdapter(adapter);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        if(v==menuIcon){
            if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                drawerLayout.closeDrawer(GravityCompat.START);
            } else {
                drawerLayout.openDrawer(GravityCompat.START);
                if (!drawerLayout.isDrawerOpen(GravityCompat.START)) {
                    drawerLayout.openDrawer(GravityCompat.START);
                }
            }
        }
        if(v==notify){
            AddValueDialog.getInstance().createDialog(MainActivity.this);
        }
        if(v==deleteView){
            Intent i = new Intent(MainActivity.this, DeleteActivity.class);
            startActivity(i);
            overridePendingTransition(0, 0);
        }
        if(v==expiredView){
            Intent i = new Intent(MainActivity.this, DeleteActivity.class);
            startActivity(i);
            overridePendingTransition(0, 0);
        }
        if(v==customizeView){
            Intent i = new Intent(MainActivity.this, DeleteActivity.class);
            startActivity(i);
            overridePendingTransition(0, 0);
        }
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return null;
        }
    }
}
