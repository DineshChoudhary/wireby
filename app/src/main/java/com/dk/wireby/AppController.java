package com.dk.wireby;

import android.app.Activity;
import android.app.Application;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.text.Spanned;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.dk.wireby.customs.CustomProgressDialog;
import com.loopj.android.http.RequestParams;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OptionalDataException;
import java.io.StreamCorruptedException;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import cz.msebera.android.httpclient.Header;

/**
 *  Created by Dinesh Choudhary on 06-Feb-17.
 */
public class AppController extends Application {
    private static Context context;
    private static AppController mInstance;
    private static CustomProgressDialog pd;
    private static  AlertDialog dialog;
//    public static ImageLoader loader;
//    public ImageLoaderConfiguration config;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        context = getApplicationContext();
//        loader = ImageLoader.getInstance();
//        setImageLoaderConfig();
//        loader.init(ImageLoaderConfiguration.createDefault(this));
    }

    public static synchronized AppController getInstance() {
        return mInstance;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
//        MultiDex.install(this);
    }

    public static Context getAppContext() {
        return AppController.context;
    }

    public void setConnectivityListener(InternetReceiver.ConnectivityReceiverListener listener) {
        InternetReceiver.connectivityReceiverListener = listener;
    }

    public static boolean activityVisible;

    public static void spinnerStart(Context context, String text) {
        String pleaseWait = text;
        try {
            if (pd != null) {
                if (pd.isShowing()) {
                    pd.dismiss();
                }
            }
            pd = new CustomProgressDialog(context, R.drawable.ic_progess);
            if (text.isEmpty())
                pd.show();
            else {
                pd.setTitle(text);
                pd.show();
            }

        } catch (Exception ee) {
            ee.printStackTrace();
        }
    }

    public static void spinnerStop() {
        try {
            if (pd != null) {
                if (pd.isShowing()) {
                    pd.dismiss();
                }
            }
        } catch (Exception ee) {
            ee.printStackTrace();
        }
    }

    public static String getDeviceId(){
      String deviceId = Settings.Secure.getString(getInstance().getContentResolver(), Settings.Secure.ANDROID_ID);
        return deviceId;
    }

   /* public void setImageLoaderConfig() {
        config = new ImageLoaderConfiguration.Builder(getApplicationContext())

                .memoryCacheSize(100 * 1024 * 1024)// 100 Mb
                .memoryCache(new LruMemoryCache(100 * 1024 * 1024))
                .defaultDisplayImageOptions(DisplayImageOptions.createSimple())
                .tasksProcessingOrder(QueueProcessingType.LIFO).build();
        loader.getInstance().init(config);
    }

    public static DisplayImageOptions OPTIONS_ListView = new DisplayImageOptions.Builder()
            .showImageForEmptyUri(R.drawable.welcome_logo)
            .showImageOnFail(R.drawable.welcome_logo)
            .showImageOnLoading(R.drawable.welcome_logo)
            .considerExifParams(true)
            .cacheInMemory(true)
            .cacheOnDisk(true)
            .imageScaleType(ImageScaleType.EXACTLY)
            .bitmapConfig(Bitmap.Config.RGB_565).build();
*/

    public static Spanned stripHtml(String html) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
        } else {
            return Html.fromHtml(html);
        }
    }

    public boolean isOnline(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    public static void hideSoftKeyboard(View view, Context mConext) {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) mConext.getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch (Exception ee) {
        }
    }

    public static void openSoftKeyboard(Activity mConext) {
        InputMethodManager imm = (InputMethodManager) mConext.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }

    public static void openSoftKeyboard(View view, Activity mConext) {
        InputMethodManager imm = (InputMethodManager) mConext.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInputFromWindow(
                view.getApplicationWindowToken(),
                InputMethodManager.SHOW_FORCED, 0);
    }

    public static void hideSoftKeyboard(Activity activity) {
        try {
            InputMethodManager imm = (InputMethodManager) activity
                    .getSystemService(Activity.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        } catch (Exception ee) {}
    }

    public static void setupUI(View view, final Context mContext) {

        //Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {

            view.setOnTouchListener(new View.OnTouchListener() {

                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard(v, mContext);
                    return false;
                }

            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {

            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {

                View innerView = ((ViewGroup) view).getChildAt(i);

                setupUI(innerView, mContext);
            }
        }
    }

    public static void popMessage(String titleMsg, String errorMsg, Context context) {
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
             builder.setTitle(titleMsg)
//                    .setIcon(R.drawable.appicon)
                    .setMessage(errorMsg)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });

            AlertDialog alert = builder.create();
            alert.setCanceledOnTouchOutside(false);
            alert.show();
        } catch (Exception ee) {

        }
    }

    public static String convertStringToUTF8(String s) {
        String out = null;
        try {
            out = new String(s.getBytes("UTF-8"), "ISO-8859-1");
        } catch (java.io.UnsupportedEncodingException e) {
            return null;
        }
        return out;
    }

    public static String getFormattedCurrency(String price, int decimalFormat) {
        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
        DecimalFormatSymbols symbols = formatter.getDecimalFormatSymbols();

        symbols.setGroupingSeparator(',');
        symbols.setDecimalSeparator('.');
        formatter.setDecimalFormatSymbols(symbols);
        formatter.setMinimumFractionDigits(decimalFormat);
        formatter.setMaximumFractionDigits(decimalFormat);
        formatter.setRoundingMode(RoundingMode.HALF_UP);
        String pric = formatter.format(Double.parseDouble(price));
        return pric;
    }

    public static long getUTCdatetime() {
        return System.currentTimeMillis();
    }

    public static String ChangeDateFormat(String DateString) {
        String sDate1=DateString;  //2018-10-09 09:34:07
        try {
            Date date1=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(sDate1);
            SimpleDateFormat dateFormatNew = new SimpleDateFormat("dd-mm-yyyy");
            dateFormatNew.setTimeZone(TimeZone.getTimeZone("UTC"));
            sDate1 = dateFormatNew.format(date1);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return sDate1;
    }

    public static boolean isEODpast(String DateString) {
        String sDate1 = DateString;  //2018-10-09 09:34:07
        boolean isEODpast = false;
        try {
            Date date1=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(sDate1);
            Calendar cal1 = Calendar.getInstance();
            Calendar cal2 = Calendar.getInstance();
            cal1.setTime(date1);
            cal2.setTime(new Date());
            boolean sameDay = cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR) &&
                    cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR);

            long milliseconds = date1.getTime();
            long diff = milliseconds - (new Date()).getTime();
            if(!sameDay && diff<0)
                isEODpast =true;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return isEODpast;
    }
}
