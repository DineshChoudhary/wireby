package com.dk.wireby;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.dk.wireby.customs.CustomActivity;

public class LoginActivity extends CustomActivity {

    TextView signin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        signin = (TextView)findViewById(R.id.signin);
        setTouchNClick(signin);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        if(v==signin)
        {
            Intent i = new Intent(LoginActivity.this,MainActivity.class);
            startActivity(i);
            overridePendingTransition(0,0);
        }
    }
}
