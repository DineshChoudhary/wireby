package com.dk.wireby;

import android.content.DialogInterface;
import android.content.Intent;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;

public class SplashActivity extends AppCompatActivity {
    protected int _splashTime = 2000;
    private Thread splashTread;
    String deviceid;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        deviceid = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        final int flags = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        getWindow().getDecorView().setSystemUiVisibility(flags);

        final View decorView = getWindow().getDecorView();
        decorView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
            @Override
            public void onSystemUiVisibilityChange(int visibility) {
                if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
                    decorView.setSystemUiVisibility(flags);
                }
            }
        });
        String crash = getIntent()
                .getStringExtra(ExceptionHandler.CRASH_REPORT);
        if (crash == null) {
            startSplash();
        } else
            showCrashDialog(crash);
    }

    private void startSplash() {
        splashTread = new Thread() {
            @Override
            public void run() {
                try {
                    synchronized (this) {
                        wait(_splashTime);
                    }
                } catch (InterruptedException e) {
                } finally {
                    if (!isFinishing()) {
                        launchActivity();
                    }
                }
            }
        };
        splashTread.start();
    }

    @Override
    public void onBackPressed() {
        finishAffinity();
    }

    private void launchActivity() {
        try {
//            if (AppController.getInstance().readLOGINDATA().getID() == 0) {
                gotoSign();
//            } else {
//                SplashActivity.this.runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        Intent i = new Intent(SplashActivity.this, MainActivity.class);
//                        startActivity(i);
//                        overridePendingTransition(0, 0);
//                        finish();
//                    }
//                });
//            }
        } catch (Exception ee) {
            gotoSign();
        }
    }


    private void gotoSign() {
        Intent i = new Intent(SplashActivity.this, LoginActivity.class);
        startActivity(i);
        overridePendingTransition(0, 0);
        finish();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            try {
                synchronized (splashTread) {
                    splashTread.notifyAll();
                }
            } catch (Exception ee) {
                finishAffinity();
            }
        }
        return true;
    }

    public void showCrashDialog(final String report) {

        AlertDialog.Builder b = new AlertDialog.Builder(this);
        b.setTitle("App Crashed");
        b.setMessage("Oops! The app crashed due to below reason:\n\n" + report);

        DialogInterface.OnClickListener ocl = new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                if (which == DialogInterface.BUTTON_POSITIVE) {
                    dialog.dismiss();
                    Intent i = new Intent(Intent.ACTION_SEND);
                    i.setType("text/html");
                    i.putExtra(Intent.EXTRA_EMAIL,
                            new String[]{"er.dineshbalayan@gmail.com"});
                    i.putExtra(Intent.EXTRA_TEXT, report);
                    i.putExtra(Intent.EXTRA_SUBJECT, "App Crashed");
                    startActivity(Intent.createChooser(i, "Send Mail via:"));
                    finish();
                } else {
                    dialog.dismiss();
                    startSplash();
                }
            }
        };
        b.setCancelable(false);
        b.setPositiveButton("Email this error", ocl);
        b.setNegativeButton("Restart", ocl);
        b.create().show();
    }
}
