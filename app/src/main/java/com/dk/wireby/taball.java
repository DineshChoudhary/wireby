package com.dk.wireby;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class taball extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    RecyclerView recyclerView;
    PostAdapter pbAdapter;

    public taball() {
        // Required empty public constructor
    }
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment taball.
     */
    // TODO: Rename and change types and number of parameters
    public static taball newInstance(String param1, String param2) {
        taball fragment = new taball();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_taball, container, false);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initui(view);
    }
    List<PostPojo>listpp=new ArrayList<>();
    private void initui(View view){
        createList();
       recyclerView=(RecyclerView) view.findViewById(R.id.rview);
       recyclerView.setHasFixedSize(true);
       LinearLayoutManager lmb=new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false);
       recyclerView.setLayoutManager(lmb);
       pbAdapter=new PostAdapter(getActivity(),listpp);
       recyclerView.setAdapter(pbAdapter);
       pbAdapter.notifyDataSetChanged();
    }

    private void createList() {
        listpp=new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            PostPojo pp=new PostPojo("Nirmal","First Time Android Mata fori"+i);
            listpp.add(pp);
        }

    }

}
